const path = require('path');

module.exports = {
  css: {
    loaderOptions: {
      stylus: {
        import: [
          path.resolve(__dirname, 'src/assets/styles/variables.styl'),
        ],
      },
    },
  },

  lintOnSave: false,

  pwa: {
    name: 'Currency Converter',
    themeColor: '#090f1e',
    msTileColor: '#090f1e',
    appleMobileWebAppStatusBarStyle: 'black-translucent',
    appleMobileWebAppCapable: 'yes',
    manifestOptions: {
      start_url: './',
      display: 'standalone',
      background_color: '#090f1e',
      description: 'Simple currency converter',
      orientation: 'portrait',
    },
  },
};
