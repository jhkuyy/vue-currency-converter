// eslint-disable-next-line no-restricted-globals
self.addEventListener('fetch', (event) => {
  const url = new URL(event.request.url);

  // if not self host
  // eslint-disable-next-line no-restricted-globals
  if (location.host !== url.host) {
    event.respondWith(
      caches.open('mysite')
        .then((cache) => fetch(event.request)
          .then((response) => {
            cache.put(event.request, response.clone());
            return response;
          })
          .catch(() => caches.match(event.request))),
    );
  } else {
    event.respondWith(
      caches.match(event.request)
        .then((cachedResponse) => {
          // Cache hit - return response
          if (cachedResponse) {
            return cachedResponse;
          }

          return fetch(event.request).then((response) => {
            // Check if we received a valid response
            if (!response || response.status !== 200 || response.type !== 'basic') {
              return response;
            }

            const responseToCache = response.clone();

            caches.open('mysite')
              .then((cache) => {
                cache.put(event.request, responseToCache);
              });

            return response;
          });
        }),
    );
  }
});
