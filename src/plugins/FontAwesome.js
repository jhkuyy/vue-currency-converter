import Vue from 'vue';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faAngleDown, faStar, faArrowLeft, faCog, faHome, faSpinner,
} from '@fortawesome/free-solid-svg-icons';

library.add(faAngleDown, faStar, faArrowLeft, faCog, faHome, faSpinner);

Vue.component('FontAwesomeIcon', FontAwesomeIcon);
