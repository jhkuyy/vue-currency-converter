if ('serviceWorker' in navigator) {
  window.addEventListener('load', () => {
    navigator.serviceWorker.register(`/service-worker.js?d=${Date.now()}`, {
      scope: '/',
    })
      .then((registration) => {
        console.log('Service worker registration succeeded:', registration);
      })
      .catch((error) => {
        console.log('Service worker registration failed:', error);
      });
  });
}
