const config = {
  baseURL: 'https://api.exchangeratesapi.io',
};

const fetchExchangeRates = async (baseCurrency) => {
  const response = await fetch(
    `${config.baseURL}/latest?base=${baseCurrency}`,
  ).then((data) => data.json());

  return response.rates;
};


export {
  fetchExchangeRates,
};
