export class AppStorage {
  constructor(storage) {
    this.storage = storage || window.localStorage;

    if (!this.isSupported()) {
      throw new Error('Storage is not supported by browser!');
    }
  }

  setItem(key, value) {
    this.storage.setItem(key, JSON.stringify(value));
  }

  getItem(key) {
    return JSON.parse(this.storage.getItem(key));
  }

  removeItem(key) {
    this.storage.removeItem(key);
  }

  clear() {
    this.storage.clear();
  }

  isSupported() {
    return !!this.storage;
  }
}

AppStorage.CALCULATOR_VALUE = 'CALCULATOR_VALUE';
AppStorage.BASE_CURRENCY = 'BASE_CURRENCY';
AppStorage.FAVORITES = 'FAVORITES';
AppStorage.CURRENT_LANG = 'CURRENT_LANG';
AppStorage.CURRENT_THEME = 'CURRENT_THEME';

const appStorage = new AppStorage();

export default appStorage;
