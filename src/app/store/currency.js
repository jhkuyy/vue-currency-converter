import appStorage, { AppStorage } from '../services/appStorage';
import { fetchExchangeRates } from '../services/currencyHttpClient';


export default {
  namespaced: true,

  state: {
    loading: false,
    currencyMap: {},
    baseCurrency: null,
    error: null,
  },

  getters: {
    currencies: (state) => Object.entries(state.currencyMap).map(([code, value]) => ({ code, value })),
  },

  mutations: {
    currencyDataRequest(state) {
      Object.assign(state, { loading: true, error: null });
    },

    currencyDataSuccess(state, payload) {
      Object.assign(state, { loading: false, currencyMap: payload });
    },

    currencyDataError(state, payload) {
      Object.assign(state, {
        loading: false,
        currencyMap: {},
        error: payload || true,
      });
    },

    setBaseCurrency(state, payload) {
      state.baseCurrency = payload;
      appStorage.setItem(AppStorage.BASE_CURRENCY, payload);
    },
  },

  actions: {
    async fetchCurrencyData({ state, commit }) {
      commit('currencyDataRequest');

      try {
        const result = await fetchExchangeRates(state.baseCurrency);

        commit('currencyDataSuccess', result);

        return result;
      } catch (e) {
        commit('currencyDataError', e);
      }
    },
  },
};
