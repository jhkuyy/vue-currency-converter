import appStorage, { AppStorage } from '../services/appStorage';
import Dictionaries from '../i18n/Dictionaries';


export default {
  namespaced: true,

  state: {
    lang: null,
  },

  getters: {
    dictionary: (state) => Dictionaries.getDictionaryByLocale(state.lang),
  },

  mutations: {
    setLang(state, lang) {
      state.lang = lang;
      appStorage.setItem(AppStorage.CURRENT_LANG, lang);
    },
  },
};
