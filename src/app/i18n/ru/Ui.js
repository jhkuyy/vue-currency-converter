import DictionaryMapping from '../DictionaryMapping';

export default {
  [DictionaryMapping.Ui.ENTER_VALUE]: 'Введите сумму',
  [DictionaryMapping.Ui.CHOOSE_CURRENCY]: 'Выберите валюту',
  [DictionaryMapping.Ui.CHOOSE_LANGUAGE]: 'Выберите язык',
  [DictionaryMapping.Ui.CHOOSE_COLOR_THEME]: 'Выберите цветовую схему',
  [DictionaryMapping.ScreenTitle.CURRENCIES]: 'Список валют',
  [DictionaryMapping.ScreenTitle.SETTINGS]: 'Настройки',
  [DictionaryMapping.Tabs.CONVERTER]: 'Конвертер',
  [DictionaryMapping.Tabs.SETTINGS]: 'Настройки',
  [DictionaryMapping.ColorTheme.DARK]: 'Темная',
  [DictionaryMapping.ColorTheme.LIGHT]: 'Светлая',
  [DictionaryMapping.Ui.TRY_AGAIN]: 'Попробовать снова',
  [DictionaryMapping.Ui.LOADING_ERROR]: 'При загрузке произошла ошибка',
};
