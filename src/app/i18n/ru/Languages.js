import DictionaryMapping from '../DictionaryMapping';

export default {
  [DictionaryMapping.Languages.RUSSIAN]: 'Русский',
  [DictionaryMapping.Languages.ENGLISH]: 'Английский',
};
