import en from './en';
import ru from './ru';

export default {
  dictionary: {
    en,
    ru,
  },
  getDictionaryByLocale(locale) {
    if (locale in this.dictionary) {
      return this.dictionary[locale];
    }

    throw new Error('Locale not supported');
  },
  getTranslationByDictionaryKey(locale, key) {
    return this.dictionary[locale][key];
  },
};
