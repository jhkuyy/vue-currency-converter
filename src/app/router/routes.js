export const Route = {
  HOME: 'home',
  CURRENCY_SETTINGS: 'currency-settings',
  SETTINGS: 'settings',
};

export const Tabs = [
  {
    route: Route.HOME,
    icon: 'home',
  },
  {
    route: Route.SETTINGS,
    icon: 'cog',
  },
];
